import { LokiJsDbAdapter } from './lib/LokiJsDbAdapter'


export * from './lib/ILokiJsDbOptions'
export * from './lib/LokiJsDbOptions'
export * from './lib/LokiJsDbAdapter'

if(!module.parent && !(process.env.NODE_ENV || '').startsWith('dev')) {
	console.error('This package is not directly runnable.\nPlease refer to the README.')
	process.exit(0)
}

LokiJsDbAdapter.register()
