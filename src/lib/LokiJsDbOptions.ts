import { IDatabaseAdapterInfo } from 'database-adapter'
import { ILokiJsDbOptions } from './ILokiJsDbOptions'


export interface LokiJsDbOptions extends IDatabaseAdapterInfo<ILokiJsDbOptions> {
	type: 'loki',
	config: ILokiJsDbOptions
}
