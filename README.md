# database-adapter-lokijs [![NPM version](https://img.shields.io/npm/v/database-adapter-lokijs.svg)](https://www.npmjs.com/package/database-adapter-lokijs)
> An adapter plugin to [database-adapter](https://www.npmjs.com/package/database-adapter) for [lokijs](https://www.npmjs.com/package/lokijs).

## Notice
Due to the incompleteness of [database-adapter](https://www.npmjs.com/package/database-adapter),
this plugin does not do much at the moment and you have to use raw
lokijs methods in order to get anywhere. 
